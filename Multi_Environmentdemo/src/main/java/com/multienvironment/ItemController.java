package com.multienvironment;

import com.multienvironment.bean.Item;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by loni on 2017/10/23.
 */
@RestController
@CrossOrigin
@RequestMapping("/item")
public class ItemController {
    @RequestMapping(value = "/getItemList", method = RequestMethod.GET)
    public List<Item> getItemList() throws Exception {
        return ItemStore.getItemList();
    }

    @RequestMapping(value = "/addItem", method = RequestMethod.POST)
    public String addItem(@RequestBody Item item) throws Exception {
        ItemStore.addItem(item);
        return "添加成功";
    }

    @RequestMapping(value = "/delItem/{id}", method = RequestMethod.GET)
    public String delItem(@PathVariable Long id) throws Exception {
        boolean isDelect = ItemStore.delItem(id);
        if (isDelect) {
            return "删除成功";
        } else {
            return "删除的元素不存在";
        }
    }
}
