package com.multienvironment.bean;

import java.io.Serializable;
import java.util.Date;

public class Item implements Serializable {
    private static final long serialVersionUID = -391632760389848971L;
    private Long id;
    private String name;
    private String ctime;

    public Item() {
    }

    public Item(Long id, String name, String ctime) {
        this.id = id;
        this.name = name;
        this.ctime = ctime;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCtime() {
        return ctime;
    }

    public void setCtime(String ctime) {
        this.ctime = ctime;
    }
}
