package com.multienvironment;

import com.multienvironment.bean.Item;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ItemStore {
    private static List<Item> itemList;

    private static void init() {
        itemList = new ArrayList<>();
        itemList.add(new Item(1L, "宝马", "2017-12-21"));
        itemList.add(new Item(2L, "奔驰", "2017-12-21"));
        itemList.add(new Item(3L, "奥迪", "2017-12-21"));
    }

    public static List<Item> getItemList() {
        if (itemList == null) {
            init();
        }
        return itemList;
    }

    public static void addItem(Item item) {
        if (itemList == null) {
            init();
        }
        itemList.add(item);
    }

    public static boolean delItem(Long id) {
        if (itemList == null) {
            init();
        }
        boolean mark = false;
        List<Item> tempList = new ArrayList<>();
        tempList.addAll(itemList);
        for (Item item : tempList) {
            if (item.getId().equals(id)) {
                itemList.remove(item);
                mark = true;
            }
        }
        return mark;
    }
}
